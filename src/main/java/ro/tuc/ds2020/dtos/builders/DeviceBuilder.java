package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.DeviceDetailsDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Person;

public class DeviceBuilder {
    public DeviceBuilder() {
    }

    public static DeviceDetailsDTO toDeviceDTO(Device device){
        return new DeviceDetailsDTO(device.getId(), device.getDescription(), device.getAddress(), device.getMaxValue(), device.getAvgValue(), device.getUserID());
    }

    public static Device toEntity(DeviceDetailsDTO deviceDetailsDTO) {
        return new Device(deviceDetailsDTO.getDescription(), deviceDetailsDTO.getAddress(), deviceDetailsDTO.getMaxValue(), deviceDetailsDTO.getAvgValue(), deviceDetailsDTO.getUserID());
    }
}
