package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

public class DeviceDetailsDTO extends RepresentationModel<DeviceDetailsDTO> {

    private UUID id;
    @NotNull
    private String description;
    @NotNull
    private String address;
    @NotNull
    private int maxValue;
    @NotNull
    private int avgValue;
    @NotNull
    private UUID userID;

    public DeviceDetailsDTO(){

    }

    public DeviceDetailsDTO(@NotNull String description, @NotNull String address, @NotNull int maxValue, @NotNull int avgValue, @NotNull UUID userID) {
        this.description = description;
        this.address = address;
        this.maxValue = maxValue;
        this.avgValue = avgValue;
        this.userID = userID;
    }

    public DeviceDetailsDTO(UUID id, @NotNull String description, @NotNull String address, @NotNull int maxValue, @NotNull int avgValue, @NotNull UUID userID) {
        this.id = id;
        this.description = description;
        this.address = address;
        this.maxValue = maxValue;
        this.avgValue = avgValue;
        this.userID = userID;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    public int getAvgValue() {
        return avgValue;
    }

    public void setAvgValue(int avgValue) {
        this.avgValue = avgValue;
    }

    public UUID getUserID() {
        return userID;
    }

    public void setUserID(UUID userID) {
        this.userID = userID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        DeviceDetailsDTO deviceDetailsDTO = (DeviceDetailsDTO) o;
        return maxValue == deviceDetailsDTO.maxValue && avgValue == deviceDetailsDTO.avgValue && id.equals(deviceDetailsDTO.id) && description.equals(deviceDetailsDTO.description) && address.equals(deviceDetailsDTO.address) && userID.equals(deviceDetailsDTO.userID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, description, address, maxValue, avgValue, userID);
    }
}
