package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.entities.Admin;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.repositories.AdminRepository;
import ro.tuc.ds2020.repositories.PersonRepository;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class AdminService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    private final AdminRepository adminRepository;

    public AdminService(AdminRepository adminRepository) {
        this.adminRepository = adminRepository;
    }

    public Admin findAdmin(String username) {
        Admin admin = adminRepository.findByUsername(username);
        return admin;
    }

//    public void addAdmin(Admin a)
//    {
//        adminRepository.save(a);
//    }
}
