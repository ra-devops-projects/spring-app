package ro.tuc.ds2020.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.LoginRequest;
import ro.tuc.ds2020.dtos.MessageResponse;
import ro.tuc.ds2020.entities.Admin;
import ro.tuc.ds2020.services.AdminService;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@RestController
@CrossOrigin
public class AdminController {
    private final AdminService adminService;

    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @PostMapping("/login")
    @ResponseBody
    public ResponseEntity<?> processLoginForm(@RequestBody LoginRequest loginRequest) {

        System.out.println(loginRequest.getUsername() + " " + loginRequest.getPassword());
        Admin admin = adminService.findAdmin(loginRequest.getUsername());

        String password= loginRequest.getPassword();
        String newPassword = hashPassword(password);

        //Admin a = new Admin(loginRequest.getUsername(), newPassword);
        //adminService.addAdmin(a);

        if (admin==null || !admin.getPassword().equals(newPassword))
            return ResponseEntity.badRequest().body(new MessageResponse("Wrong credentials"));
        else
            return ResponseEntity.ok(admin);

    }

    public String hashPassword(String pw)
    {
        byte[] bytesOfPassword=pw.getBytes(StandardCharsets.UTF_8);
        byte[] hashedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            hashedPassword = md.digest(bytesOfPassword);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        String newPassword=new String(hashedPassword);
        return newPassword;
    }
}
