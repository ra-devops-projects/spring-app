package ro.tuc.ds2020.controllers;

import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.services.AdminService;
import ro.tuc.ds2020.services.PersonService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
public class PersonController {

    private final PersonService personService;
    private final AdminService adminService;

    public PersonController(PersonService personService, AdminService adminService) {
        this.personService = personService;
        this.adminService = adminService;
    }

    @GetMapping(value = "/persons")
    public ResponseEntity<List<PersonDetailsDTO>> getPersons() {
        List<PersonDetailsDTO> dtos = personService.findPersons();
        for (PersonDetailsDTO dto : dtos) {
            Link personLink = linkTo(methodOn(PersonController.class)
                    .getPerson(dto.getId())).withRel("personDetails");
            dto.add(personLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("/persons")
    public ResponseEntity<PersonDetailsDTO> insertPerson(@Valid @RequestBody PersonDetailsDTO personDTO) {
        Person person = personService.insert(personDTO);
        return new ResponseEntity<>(PersonBuilder.toPersonDetailsDTO(person), HttpStatus.CREATED);
    }

    @GetMapping(value = "/persons/{id}")
    public ResponseEntity<PersonDetailsDTO> getPerson(@PathVariable("id") UUID personId) {
        PersonDetailsDTO dto = personService.findPersonById(personId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping("/persons/{id}")
    public ResponseEntity deleteClientById(
            @PathVariable UUID id) {
        personService.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PutMapping("/persons/{id}")
    public ResponseEntity<PersonDetailsDTO> updateClient(
            @PathVariable("id") UUID personId,
            @RequestBody PersonDetailsDTO dto) {
        return ResponseEntity.status(HttpStatus.OK).body(personService.update(personId,dto));
    }

}
