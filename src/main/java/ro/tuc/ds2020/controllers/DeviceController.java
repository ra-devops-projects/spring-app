package ro.tuc.ds2020.controllers;

import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.DeviceDetailsDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.services.DeviceService;
import ro.tuc.ds2020.services.PersonService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
public class DeviceController {
    private final DeviceService deviceService;
    private final PersonService personService;

    public DeviceController(DeviceService deviceService, PersonService personService) {
        this.deviceService = deviceService;
        this.personService = personService;
    }

    @GetMapping(value = "/devices")
    public ResponseEntity<List<DeviceDetailsDTO>> getDevices() {
        List<DeviceDetailsDTO> dtos = deviceService.findDevices();
        for (DeviceDetailsDTO dto : dtos) {
            Link deviceLink = linkTo(methodOn(DeviceController.class)
                    .getDevices()).withRel("deviceDetails");
            dto.add(deviceLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("/devices")
    public ResponseEntity<DeviceDetailsDTO> insertDevice(@Valid @RequestBody DeviceDetailsDTO deviceDetailsDTO) {
        System.out.println(deviceDetailsDTO.getAddress());
        Device device = deviceService.insert(deviceDetailsDTO);

        return new ResponseEntity<>(DeviceBuilder.toDeviceDTO(device), HttpStatus.CREATED);
    }

    @DeleteMapping("/devices/{id}")
    public ResponseEntity deleteDeviceById(
            @PathVariable UUID id) {
        deviceService.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PutMapping("/devices/{id}")
    public ResponseEntity<DeviceDetailsDTO> updateDevice(
            @PathVariable("id") UUID deviceId,
            @RequestBody DeviceDetailsDTO dto) {
        return ResponseEntity.status(HttpStatus.OK).body(deviceService.update(deviceId,dto));
    }
}
